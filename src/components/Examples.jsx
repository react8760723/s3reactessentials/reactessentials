import { EXAMPLES } from '../data.js';
import TabButton from './TabButton.jsx';
import { useState } from 'react';

export default function Examples() {
  const [selectedTopic, setSelectedTopic] = useState();

  function handleSelect(selectedTab) {
    setSelectedTopic(EXAMPLES[selectedTab]);
  }
  return (
    <section id='examples'>
      <h2>Examples</h2>
      <menu>
        {Object.keys(EXAMPLES).map((key) => (
          <TabButton
            key={key}
            isSelected={selectedTopic === EXAMPLES[key]}
            onSelect={() => handleSelect(key)}
          >
            {EXAMPLES[key].title}
          </TabButton>
        ))}
      </menu>
      {selectedTopic ? (
        <div id='tab-content'>
          <h3>{selectedTopic.title}</h3>
          <p>{selectedTopic.description}</p>
          <pre>
            <code>{selectedTopic.code}</code>
          </pre>
        </div>
      ) : (
        <p>Please click a button</p>
      )}
    </section>
  );
}
